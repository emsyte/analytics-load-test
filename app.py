#!/usr/bin/env python3

from aws_cdk import core

from analytics_load_test.analytics_load_test_stack import AnalyticsLoadTestStack


app = core.App()
AnalyticsLoadTestStack(app, "analytics-load-test")

app.synth()
