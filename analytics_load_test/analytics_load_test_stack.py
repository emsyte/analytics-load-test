from aws_cdk import aws_ecr_assets as ecr_assets
from aws_cdk import aws_ecs as ecs
from aws_cdk import aws_dynamodb as dynamodb
from aws_cdk import aws_lambda as lambda_
from aws_cdk import aws_iam as iam
from aws_cdk import aws_apigateway as apigateway
from aws_cdk import core
from pathlib import Path


class AnalyticsLoadTestStack(core.Stack):
    def __init__(self, scope: core.Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        analytics_result_table = dynamodb.Table(
            self,
            "analytics-result-table",
            partition_key={
                "name": "observation_key",
                "type": dynamodb.AttributeType.STRING,
            },
            billing_mode=dynamodb.BillingMode.PAY_PER_REQUEST,
        )

        api = apigateway.RestApi(
            self,
            "AnalyticsLoadTestApi",
            rest_api_name="Analytics Load Test Api",
            description="Api to test the load of the analytics Api",
        )

        log_report_function = lambda_.Function(
            self,
            "LogReportFunction",
            runtime=lambda_.Runtime.PYTHON_3_8,
            handler="log_report.handler",
            code=lambda_.Code.asset("src/log_report"),
            environment={"TABLE_NAME": analytics_result_table.table_name},
        )

        log_report_function.add_to_role_policy(
            iam.PolicyStatement(
                actions=["dynamodb:*"], resources=[analytics_result_table.table_arn]
            )
        )

        api.root.add_method(
            "POST",
            apigateway.LambdaIntegration(
                log_report_function,
                request_templates={"application/json": '{ "statusCode": "200" }'},
            ),
        )

        analytics_load_test = ecs.FargateTaskDefinition(self, "AnalyticsLoadTest")
        analytics_load_test.add_to_task_role_policy(
            iam.PolicyStatement(
                actions=["dynamodb:*"], resources=[analytics_result_table.table_arn]
            )
        )

        analytics_load_test.add_to_task_role_policy(
            iam.PolicyStatement(actions=["s3:*"], resources=["*"])
        )

        analytics_load_test_image = ecr_assets.DockerImageAsset(
            self,
            "AnalyticsLoadTestImage",
            directory=str(Path(__file__).parent.parent / "src/analytics_load_test"),
        )

        analytics_load_test.add_container(
            "DefaultContainer",
            image=ecs.ContainerImage.from_docker_image_asset(analytics_load_test_image),
            memory_limit_mib=128,
            logging=ecs.AwsLogDriver(stream_prefix="AnalyticsLoadTestTask"),
            environment={
                "TABLE_NAME": analytics_result_table.table_name,
                "CALLBACK_URL": api.url,
                "ITERATIONS": "5",
            },
        )
