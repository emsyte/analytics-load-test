import json
import os
from datetime import datetime, timezone

import boto3

TABLE_NAME = os.environ["TABLE_NAME"]

dynamodb = boto3.resource("dynamodb")

table = dynamodb.Table(TABLE_NAME)


def handler(event, context):
    data = json.loads(event["body"])

    observation_key = data["key"]
    item = table.get_item(Key={"observation_key": observation_key})

    if item["Item"]["status"] != "finalized":
        print(item["Item"])

        return {
            "statusCode": 200,
            "body": "{}",
            "headers": {"ContentType": "application/json"},
        }

    started = datetime.fromisoformat(item["Item"]["started"])
    finished = datetime.now(tz=timezone.utc)

    item["Item"]["status"] = "complete"

    item["Item"]["finished"] = finished.isoformat()
    item["Item"]["duration_seconds"] = int((finished - started).total_seconds())

    table.put_item(Item=item["Item"])

    return {
        "statusCode": 200,
        "body": "{}",
        "headers": {"ContentType": "application/json"},
    }
