import concurrent.futures
import subprocess
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime, timezone

import boto3
import requests
from environs import Env

env = Env()
env.read_env()

TABLE_NAME = env("TABLE_NAME")
ITERATIONS = env.int("ITERATIONS")
CALLBACK_URL = env("CALLBACK_URL")

s3 = boto3.resource("s3")
dynamodb = boto3.resource("dynamodb")

table = dynamodb.Table(TABLE_NAME)

copy_source = {"Bucket": "emsyte-analytics-test-data", "Key": "benchmark"}


def create_observation():
    response = requests.post(
        "https://kioyre7wn5.execute-api.us-east-1.amazonaws.com/production/observation",
        json={"algorithms": ["attention", "emotion"]},
        headers={
            "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiYmY2MmE4NzYtZmY0Yy00ODlmLWFhMmQtNTI1ZGUxZWEyOGEwIn0.cMiRwH7RhommaVMb8pURX8M29UEYDUmz_l7N6vgtfk4"
        },
    )
    data = response.json()
    return data["key"]


def finalize(key):
    response = requests.post(
        f"https://kioyre7wn5.execute-api.us-east-1.amazonaws.com/production/observation/{key}/finalize",
        json={"callback": CALLBACK_URL},
        headers={
            "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiYmY2MmE4NzYtZmY0Yy00ODlmLWFhMmQtNTI1ZGUxZWEyOGEwIn0.cMiRwH7RhommaVMb8pURX8M29UEYDUmz_l7N6vgtfk4"
        },
    )
    data = response.json()
    return key, data


def copy_to_dest(observation_key):
    source = s3.Bucket("emsyte-analytics-test-data")
    dest = s3.Bucket("emsyte-media-production")

    source_prefix = "benchmark"
    dest_prefix = f"emsyte-frames/{observation_key}"

    # Pre-known number of frames
    file_count = 723

    for i, obj in enumerate(source.objects.filter(Prefix=source_prefix)):
        source_obj = {"Bucket": source.name, "Key": obj.key}

        # replace the prefix
        new_key = obj.key.replace(source_prefix, dest_prefix, 1)

        dest_obj = dest.Object(new_key)
        dest_obj.copy(source_obj)
        if i % 100 == 0:
            print(
                f"Copying Frames for Observation {observation_key}: {i + 1}/{file_count}"
            )

    print(f"Finished copying frames for observation {observation_key}")


def write_to_dynamodb(observation_key):
    table.put_item(
        Item={
            "observation_key": observation_key,
            "status": "started",
            "started": str(datetime.now(tz=timezone.utc)),
        }
    )


def update_status(observation_key):
    item = table.get_item(Key={"observation_key": observation_key})["Item"]
    item["status"] = "finalized"
    table.put_item(Item=item)
    print(f"Updated status to finalized for {observation_key}")


def test_single_observation():
    key = create_observation()
    write_to_dynamodb(key)
    copy_to_dest(key)
    finalize(key)
    update_status(key)

    return key


print(f"Running {ITERATIONS} iterations")

results = []
for i in range(ITERATIONS):
    results.append(test_single_observation())
    print(f"Completed {i+1}/{ITERATIONS}")

print(results)
